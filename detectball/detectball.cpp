/*
*  detectball
*
*  Detect a black and white ball in images
*  Copyright 2017 Domenico Daniele Bloisi
*
*  This file is part of the tutorial
*  "How to Use OpenCV for Ball Detection" by Domenico D. Bloisi
*  http://profs.scienze.univr.it/~bloisi/tutorial/balldetection.html
*  and it is distributed under the terms of the
*  GNU Lesser General Public License (Lesser GPL)
*
*  detectball is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  detectball is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with createpos.  If not, see <http://www.gnu.org/licenses/>.
*
*  This file contains a C++ OpenCV based implementation for
*  detecting a black and white ball in images as explained
*  in "How to Use OpenCV for Ball Detection" by Domenico D. Bloisi
*  http://profs.scienze.univr.it/~bloisi/tutorial/balldetection.html
*
*  Please, cite the above web page if you use createpos.
*
*  detectball has been written by Domenico D. Bloisi
*
*  Please, report suggestions/comments/bugs to
*  domenico.bloisi@gmail.com
*
*/

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include "opencv2/core/core_c.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <chrono>
#include <ctime>

#if defined(_MSC_VER)
#include <tchar.h>
#include <strsafe.h>
#include <windows.h>
#pragma comment(lib, "Ws2_32.lib")
#elif defined(__GNUC__) || defined(__GNUG__)
#include <dirent.h>
#endif

using namespace std;
using namespace cv;

int ocurrencias[4]{0,0,0,0};
int maxocurr(0);
int ballDetected(0),ballNotDetected(0),falsePositive(0);


void updateOcurrencias(int x, int y, Mat frame){
	int maxX = frame.cols;
	int maxY = frame.rows;
	int halfX = maxX/2;
	int halfY = maxY/2;
	if(x < halfX){
		if(y < halfY){
			ocurrencias[0]++;
		}else{
			ocurrencias[2]++;
		}
	}else{
		if(y < halfY){
			ocurrencias[1]++;
		}else{
			ocurrencias[3]++;
		}
	}
	for(int num: ocurrencias){
		maxocurr = max(maxocurr,num);
	}
}

void colorFrame(Mat frame){
	int rows = frame.rows;
	int cols = frame.cols;
	for(int i = 0; i < 4; ++i){
		int trow = i / 2;
		int tcol = i % 2;
		Mat tmp = frame(Rect(0,0,cols/2,rows/2));
		if(ocurrencias[i] != 0){
			float percent = float(ocurrencias[i]) / maxocurr;
			//tmp.
		}
	}
}

/** Function Headers */
void detectAndDisplay( string& img_filename );
bool detectAndDisplay(Mat frame, int num_frame, VideoWriter *vWriter);

/** Global variables */
String cascade_name = "cascade_false_alarm.xml";
CascadeClassifier cascade;

string window_name = cascade_name;//"SPQR TEAM - ball detection with LBP";

long startFrame(0);
long endFrame(LONG_MAX);
bool training(false);

enum EntryType{
	IMAGE,
	VIDEO,
	CAMERA
};

int main( int argc, const char** argv )
{
	String dirname;
	cout << "SPQR TEAM - ball detection with LBP" << endl;
	cout << endl;	
	
	EntryType entry;
	//cout << string::length(argv[3]) << endl;

	if (argc == 3 || (argc >= 4 && argc <= 7 && string(argv[3]) == "video")) {
		dirname.assign(argv[2]);
		cascade_name.assign(argv[1]);
		window_name.assign(argv[1]);
		entry = (argc == 3) ? IMAGE : VIDEO;
		if(argc >= 5){
			startFrame = stoi(argv[4]);
		}
		if(argc >= 6){
			endFrame = stoi(argv[5]);
		}
		if(argc >= 7){
			training = true;
		}
	} else if (argc == 2){
		dirname.assign("");
		cascade_name.assign(argv[1]);
		window_name.assign(argv[1]);
		entry = CAMERA;
	} else {
		cout << "Usage is:" << endl;
		cout << argv[0] << " <cascade classifier> <dirname>" << endl;
		cout << "or" << endl;
		cout << argv[0] << " <cascade classifier> <dir video> --video [startFrame] [endFrame]" << endl;
		cout << "or" << endl;
		cout << argv[0] << " <cascade classifier>" << endl;
		cout << "if you want to stream via your camera" << endl;
		return EXIT_FAILURE;
	}

	int num_frame = startFrame;
	
	//-- 1. Load the cascade
	cout << "Loading the cascade " << cascade_name << "...";
	cout.flush();
	if (!cascade.load(cascade_name)) {
		cout << endl;
		cout << "--(!)Error loading CASCADE: " << cascade_name << endl;
		return EXIT_FAILURE;
	}
	else {
		cout << "[OK]" << endl;
	}

#if defined(_MSC_VER)

	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.

	StringCchLength(dirname.c_str(), MAX_PATH, &length_of_arg);

	if (length_of_arg > (MAX_PATH - 3))
	{
		_tprintf(TEXT("\nDirectory path is too long.\n"));
		exit(EXIT_FAILURE);
	}

	_tprintf(TEXT("\nTarget directory is %s\n\n"), dirname.c_str());

	// Prepare string for use with FindFile functions.  First, copy the
	// string to a buffer, then append '\*' to the directory name.

	StringCchCopy(szDir, MAX_PATH, dirname.c_str());
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.

	hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		exit(EXIT_FAILURE);
	}

	// List all the files in the directory with some info about them.
	string img_filename;
	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			continue;
		}
		else
		{
			PTSTR pszFileName = ffd.cFileName;
			std::string name(pszFileName);
			img_filename = dirname + "\\" + name;
			detectAndDisplay(img_filename);
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		exit(EXIT_FAILURE);
	}

	FindClose(hFind);

#elif defined(__GNUC__) || defined(__GNUG__)
	cout << "Inicio" << endl;
	DIR *dir;
	struct dirent *ent;
	string outputFile;
	if (entry == IMAGE && (dir = opendir(dirname.c_str())) != NULL) {
		
		string img_filename;
		auto start = chrono::high_resolution_clock::now();
		while ((ent = readdir(dir)) != NULL) {
			img_filename = dirname + "/" + ent->d_name;
			cout << "Directorio " << img_filename << endl;
			if(ent->d_name[0] == '.') {
				cout << "Directorio invalido (/. o /..)\n";
				continue;
			}	
			detectAndDisplay(img_filename);
		}
		auto end = chrono::high_resolution_clock::now();
		auto duration = chrono::duration_cast<chrono::microseconds>(end-start);
		cout << "Duracion del programa: " << duration.count() << endl;
		closedir(dir);
	
	}else if(entry == CAMERA){
		cout << "Camara" << endl;
		VideoCapture capture;
		capture.open(0);
		if(!capture.isOpened()){
			cout << "La camara no se pudo abrir\n";
			return -1;
		}
		Mat cameraFrame;
		num_frame = 1;
		while(capture.read(cameraFrame)){
			detectAndDisplay(cameraFrame,num_frame++,nullptr);
			//waitKey(0);
		}
	} else if (entry == VIDEO){
		cout << "Video" << endl;
		VideoCapture capture(dirname);
		VideoWriter vWriter;
		outputFile = dirname;
		std::time_t t = std::time(0);   // get time now
    std::tm* now = std::localtime(&t);
    std::cout << (now->tm_year + 1900) << '-' 
         << (now->tm_mon + 1) << '-'
         <<  now->tm_mday
         << "\n";
		int y = now->tm_year + 1900;
		int m = now->tm_mon + 1;
		int d = now->tm_mday;
		int h = now->tm_hour;
		int min = now->tm_min;
		int s = now->tm_sec;
		string remplazo = to_string(y) + "-" + to_string(m) + "-" + to_string(d) + "-"
				   + to_string(h) + "-" + to_string(min) + "-" + to_string(s) + ".mp4";
		outputFile.replace(outputFile.end()-4,outputFile.end(),remplazo);
		double fps = capture.get(CAP_PROP_FPS);
		capture.set(1,startFrame);
		
		Mat videoFrame;
		vWriter.open(outputFile, VideoWriter::fourcc('M','J','P','G'), 28, Size(640, 480));
		bool signal = true;
		while(capture.read(videoFrame)){
			signal = detectAndDisplay(videoFrame,num_frame++,&vWriter);
			if(!signal){
				vWriter.release();
				break;
			}
		}
		
	}
	else {
		/* could not open directory */
		perror("");
		return EXIT_FAILURE;
	}
	if(entry != IMAGE){
		double sumaOcurrencias = 0;
		for(int num:ocurrencias){
			sumaOcurrencias += num;
		}
		string outputText = outputFile;
		outputText.replace(outputText.end()-4,outputText.end(),".txt");
		ofstream streamText;
		streamText.open(outputText);
		cout << outputText << endl;
		
		streamText << "Resultados de los cuadrantes: \n";
		streamText << fixed << setprecision(4) << ocurrencias[0]*100/sumaOcurrencias
										<< " " << ocurrencias[1]*100/sumaOcurrencias
										<< "\n" << ocurrencias[2]*100/sumaOcurrencias
										<< " " << ocurrencias[3]*100/sumaOcurrencias << endl;
		/*printf("%.5f %.5f\n%.5f %.5f\n",ocurrencias[0]*100/sumaOcurrencias,
									ocurrencias[1]*100/sumaOcurrencias,
									ocurrencias[2]*100/sumaOcurrencias,
									ocurrencias[3]*100/sumaOcurrencias);*/
		streamText << "Numero de frames analizados: " << num_frame - startFrame << endl;
		streamText << "Frame inicial: " << startFrame << endl;
		streamText << "Frame final: " << endFrame << endl;
		streamText << "Archivo analizado: " << dirname << endl;
		if(training){
			streamText << "Veces que fue detectada la bola correctamente: " << ballDetected << endl;
			streamText << "Porcentaje de detección correcta: " << 100 * double(ballDetected) / double(ballDetected + ballNotDetected) << "%" << endl;
			streamText << "Veces que fue detectada la bola incorrectamente: " << ballNotDetected << endl;
			streamText << "Porcentaje de detección incorrecta: " <<  100 * double(ballNotDetected) / double(ballDetected + ballNotDetected) << "%" << endl;
			streamText << "Cantidad de falsos positivos: " << falsePositive << endl;
			streamText << "Porcentaje de frames con falsos positivos: " << 100 * falsePositive/double(num_frame - startFrame) << "%" << endl;
		}
		double left = ocurrencias[0] + ocurrencias[2];
		double right = ocurrencias[1] + ocurrencias[3];
		double total = ocurrencias[0] + ocurrencias[1] + ocurrencias[2] + ocurrencias[3]; 
		if(left > right){
			cout << "El equipo B es el favorito, tiene una probabilidad de ganar del " << left*100/total << "%";
			streamText << "El equipo B es el favorito, tiene una probabilidad de ganar del " << left*100/total << "%";
		}else{
			cout << "El equipo A es el favorito, tiene una probabilidad de ganar del " << right*100/total << "%";
			streamText << "El equipo A es el favorito, tiene una probabilidad de ganar del " << right*100/total << "%";
		}
		cout << endl;
		streamText.flush();
		streamText.close();
		
		
		
		//cout << ocurrencias[0]*100/sumaOcurrencias << "% " << ocurrencias[1]*100/sumaOcurrencias << "%" << endl;
		//cout << ocurrencias[2]*100/sumaOcurrencias << "% " << ocurrencias[3]*100/sumaOcurrencias << "%" << endl;
		//cout << endl;
	}
#endif
	return EXIT_SUCCESS;
}


/**
* @function detectAndDisplay
*/
void detectAndDisplay(string& img_filename)
{
	Mat frame;
	frame = imread(img_filename, IMREAD_COLOR/*,CV_LOAD_IMAGE_COLOR*/);
	if (!frame.data) {
		cout << "Unable to read input frame: " << img_filename << endl;
		exit(EXIT_FAILURE);
	}
    
	Mat resized_frame(Size(640, 480), CV_8UC3);
	resize(frame, resized_frame, resized_frame.size());
	frame = resized_frame.clone();

    std::vector<Rect> balls;
    Mat frame_gray;

    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );

    cascade.detectMultiScale(frame_gray, balls, 1.1, 5, 8, Size(16, 16));

   Mat gui_frame = frame.clone();
   for( unsigned int i = 0; i < balls.size(); i++ )
   {
      Point center( balls[i].x + cvRound(balls[i].width*0.5), cvRound(balls[i].y + balls[i].height*0.5) );
      circle(gui_frame, center, cvRound(balls[i].width*0.5), Scalar( 255, 0, 255 ), 2, 8, 0 );
	  //rectangle(gui_frame,balls[i],Scalar(255,0,0),2);
	  cout << balls[i].x << ", " << balls[i].y << endl;
	  //Mat ballROI = frame_gray( balls[i] );
   } 

   //-- Show what you got
   imshow( window_name, gui_frame );
   int key = waitKey(0);
}

bool detectAndDisplay(Mat frame, int num_frame, VideoWriter *vWriter)
{
	//Mat frame;
	//frame = imread(img_filename, IMREAD_COLOR/*,CV_LOAD_IMAGE_COLOR*/);
	if (!frame.data) {
		cout << "Unable to read input frame: " << endl;
		exit(EXIT_FAILURE);
	}
    
	Mat resized_frame(Size(640, 480), CV_8UC3);
	resize(frame, resized_frame, resized_frame.size());
	frame = resized_frame.clone();

    std::vector<Rect> balls;
    Mat frame_gray;

    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );

    cascade.detectMultiScale(frame_gray, balls, 1.1, 5, 8, Size(16, 16));

	Mat gui_frame = frame.clone();
	//addText(gui_frame,"Frame " + to_string(1),Point(0,0),"Ubuntu");
	putText(gui_frame, "Frame " + to_string(num_frame), Point(30, 30), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,0),1);
	
	for( unsigned int i = 0; i < balls.size(); i++ )
	{
	   	updateOcurrencias(balls[i].x,balls[i].y,gui_frame);
		Point center( balls[i].x + cvRound(balls[i].width*0.5), cvRound(balls[i].y + balls[i].height*0.5) );
		circle(gui_frame, center, cvRound(balls[i].width*0.5), Scalar( 255, 0, 255 ), 2, 8, 0 );
		
		//rectangle(gui_frame,balls[i],Scalar(255,0,0),2);
		cout << balls[i].x << ", " << balls[i].y << endl;
		//Mat ballROI = frame_gray( balls[i] );
	} 

   	//-- Show what you got
   	imshow( window_name, gui_frame );
   
   	int key = waitKey((training?0:10));
   	if(vWriter!=nullptr)
   		vWriter->write(gui_frame);
	switch(key){
		case 'a':
			ballDetected++;
			break;
		case 's':
			ballNotDetected++;
			break;
		case 'z':
			ballDetected++;
			falsePositive++;
			break;
		case 'x':
			ballNotDetected++;
			falsePositive++;
			break;
		case 'c':
			falsePositive++;
			break;
		default:
			break;
	}
   	return key != '0' && num_frame < endFrame;
}
